Vue.use(VueAwesomeSwiper);

var manageFinancerApp = new Vue({
    el: '#manageFinancerApp',
    data: {
        currentFund: {
            title: 'Current Fund',
            stuff: 1,
            moreStuff: 123,
            selected: false,
            current: true
        },
        availableFunds: [
            {
                title: 'Fund 1',
                stuff: 1,
                moreStuff: 123,
                selected: false,
                current: false
            },
            {
                title: 'Fund 2',
                stuff: 1,
                moreStuff: 123,
                selected: false,
                current: false
            },
            {
                title: 'Fund 3',
                stuff: 1,
                moreStuff: 123,
                selected: false,
                current: false
            },
            {
                title: 'Fund 4',
                stuff: 1,
                moreStuff: 123,
                selected: false,
                current: false
            }
        ],
        fundFilters: [
            {
                title: 'Financer 1'
            },
            {
                title: 'Financer 2'
            },
            {
                title: 'Financer 3'
            }
        ],
        swiperOptions: {
            slidesPerView: 3,
            spaceBetween: 20,
            // pagination: {
            //     el: '#swiper-pagination',
            //     clickable: true
            // }
        },

    }
});