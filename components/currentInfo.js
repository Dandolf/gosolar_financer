Vue.component('current-info', {
    template: `
        <v-card>
            <v-toolbar 
                color="info" 
                dark 
                @click="show = !show"
                style="cursor: pointer;"
            >
                <v-toolbar-title>Current Financial Information</v-toolbar-title>
                <v-spacer></v-spacer>
                <v-btn
                    icon                                    
                >
                    <v-icon>{{ show ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}}</v-icon>
                </v-btn>
            </v-toolbar>
            <v-slide-y-transition>
                <v-card-text v-show="show">
                    <v-layout>
                        things
                    </v-layout>
                </v-card-text>
            </v-slide-y-transition>
        </v-card>
    `,
    data: function () {
        return {
            show: false
        }
    }
});