Vue.component('fund-card', {
    template: `
    <v-card style="margin-left: 1px; margin-right: 1px;">
        <v-toolbar :color="headerColor" dark>
            <!-- <h3>Current Fund</h3> -->
            <v-toolbar-title>{{title}}</v-toolbar-title>
        </v-toolbar>
        <v-card-text>
            <v-layout row>
                <v-flex xs8>
                    <h4>Stuff</h4>
                </v-flex>
                <v-flex xs4>
                    123
                </v-flex>
            </v-layout>
            <v-layout row>
                <v-flex xs8>
                    More Stuff
                </v-flex>
                <v-flex xs4>
                    123
                </v-flex>
            </v-layout>
        </v-card-text>
    </v-card>
    `,
    data: function () {
        return {

        }
    },
    props: ['title', 'selected', 'current'],
    computed: {
        headerColor : function () {
            if (this.current) {
                return 'success';
            } else {
                return 'blue-grey lighten-4';
            }
        }
    }
});